using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemigo : FSM
{
    [HideInInspector]
    public NPCIDLE _estado_idle;
    public EnemigoIDLE _ENestado_idle;



    public NPCESCAPE _estado_escape;
    public EnemigoAtaque _ENestado_ataque;
    public Material MaterialSinInfeccion,MaterialInfectado;
    public bool EstaInfectado;
    private Quaternion angulo;
    private float grado;
    private float cronometro;
    private int rutina;
    public float velocidad;
    public GameObject npcactivo;
    public float auxtransformacion;

    void Awake() 
    {
        _estado_idle = new NPCIDLE(this);
        _ENestado_idle = new EnemigoIDLE(this);

        _estado_escape = new NPCESCAPE(this);
        _ENestado_ataque = new EnemigoAtaque(this);
    }
    void Start()
    {
        Comenzar();
    }

    
    void Update()
    {
        Actualizar();
    }

    public void MovimientoRandom(float pVelocidad)
    {
        cronometro += Time.deltaTime;
        if(cronometro >= 2)
        {
            velocidad = +velocidad;
            rutina = Random.Range(0,1);
            cronometro = 0;
        } 

        switch (rutina)
        {
            case 0:
                grado = Random.Range(0,360);
                angulo = Quaternion.Euler(0,grado,0);
                rutina++;
                break;
            case 1:
                transform.rotation = Quaternion.RotateTowards(transform.rotation,angulo,0.5f);
                transform.Translate(Vector3.forward * pVelocidad * Time.deltaTime);
                break;
            default:
                break;
        }

    }

    private void OnCollisionEnter(Collision other) { if (other.gameObject) velocidad = -velocidad; }
    public bool infectadocerca()
    {
        foreach (GameObject item in GameManager.instancia.Infectados)
        {
            if (Vector3.Distance(this.transform.position,item.transform.position) < 5)
            {


                return true;
            }  
        }

        return false;

        

    }
    public bool npcscerca()
    {
        foreach (GameObject item in GameManager.instancia.NPCs)
        {
            if (Vector3.Distance(this.transform.position,item.transform.position) < 8)
            {
                npcactivo = item;
                return true;
               
            }  
        }

        return false;


    }
    public void transformacioninfectado()
    {
        auxtransformacion += Time.deltaTime;
        if (auxtransformacion >= 5)
        {
            EstaInfectado = true;
        }

    }
    protected override EnemigoEstadoBase IniciarEstado()
    {
        _estado_idle.Inicio();
        return _estado_idle;
    }

}
