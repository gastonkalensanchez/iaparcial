using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableroManager : MonoBehaviour
{
    public static TableroManager InstanciaTablero;
    public GameObject[,] Tablero = new GameObject[16, 16];
    public GameObject Casillero,objeto,objeto2;
    public Vector3 posj;
    void Start()
    {
        if (InstanciaTablero == null)
        {
            InstanciaTablero = this;
        }
        CrearTablero();
        InstanciarObjetos(objeto, new Vector2(15, 15));
        InstanciarObjetos(objeto2, new Vector3(8, 8));

    }
    private void CrearTablero()
    {
        for (int x = 0; x < Tablero.GetLength(0); x++)
        {
            for (int y = 0; y < Tablero.GetLength(1); y++)
            {
                // Crear una casilla
                Instantiate(Casillero, new Vector3(x, 0.0f, y), Quaternion.identity);
            }
        }
    }
    public void InstanciarObjetos(GameObject pObjeto, Vector2 pUbicacion)
    {
        Instantiate(pObjeto,new Vector3(pUbicacion.x,0.555f,pUbicacion.y),Quaternion.identity);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
