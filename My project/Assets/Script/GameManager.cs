using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject objeto, objeto2;
    void Start()
    {
        objeto = GameObject.FindGameObjectWithTag("objeto1");
        objeto2 = GameObject.FindGameObjectWithTag("objeto2");
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SeguirObjetos();
        }
        Escapar();

    }
    void SeguirObjetos()
    {
        if (objeto.transform.position.x == objeto2.transform.position.x)
        {
            if (objeto.transform.position.z < objeto2.transform.position.z)
            {
                MoverY(objeto, 1);
            }
            else
            {
                MoverY(objeto, -1);
            }
        }
        else
        {
            if (objeto.transform.position.x < objeto2.transform.position.x)
            {

                MoverY(objeto, 1);
                MoverX(objeto, 1);
            }
            else
            {
                MoverY(objeto, -1);
                MoverX(objeto, -1);
            }
        }
        
                
        

    }
    void Escapar()
    {
        //if (objeto.transform.position.x == objeto2.transform.position.x + 2)
        //    MoverX(objeto2, -1);
        //else if(objeto.transform.position.x == objeto2.transform.position.x - 2)
        //    MoverX(objeto2, 1);
        if (objeto.transform.position.z == objeto2.transform.position.z + 2)
        {
            MoverY(objeto2, -1);
            MoverX(objeto2, -1);
        }
            
        else if (objeto.transform.position.z == objeto2.transform.position.z - 2)
        {
            MoverY(objeto2, 1);
            MoverX(objeto2, 1);
        }
         

    }
    
    void MoverX(GameObject pObjeto,int dir)
    {

       
        Vector3 nuevaPosición = pObjeto.transform.position + Vector3.right * dir;
        pObjeto.transform.position = nuevaPosición;

        if (nuevaPosición.x >= 0 && nuevaPosición.x < TableroManager.InstanciaTablero.Tablero.GetLength(0) * dir &&
            nuevaPosición.z >= 0 && nuevaPosición.z < TableroManager.InstanciaTablero.Tablero.GetLength(1) * dir)
        {
            if (nuevaPosición.x < 0 || nuevaPosición.x > 8)
            {
                pObjeto.transform.position = nuevaPosición;
            }
            
        }
    }
    void MoverY(GameObject pObjeto, int dir)
    {
        Vector3 nuevaPosición = pObjeto.transform.position + Vector3.forward * dir;
        pObjeto.transform.position = nuevaPosición;

        if (nuevaPosición.x >= 0 && nuevaPosición.x < TableroManager.InstanciaTablero.Tablero.GetLength(0) * dir &&
            nuevaPosición.z >= 0 && nuevaPosición.z < TableroManager.InstanciaTablero.Tablero.GetLength(1) * dir)
        {
            pObjeto.transform.position = nuevaPosición;
        }
        
    }


}
