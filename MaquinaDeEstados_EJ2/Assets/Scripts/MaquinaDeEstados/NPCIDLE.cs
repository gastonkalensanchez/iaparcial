using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCIDLE :  EnemigoEstadoBase
{
    public NPCIDLE(MovimientoEnemigo m_e):base("npcidle",m_e){}

    public override void Inicio()
    {
        base.Inicio();
        ((MovimientoEnemigo)m_e).gameObject.GetComponent<Renderer>().material = ((MovimientoEnemigo)m_e).MaterialSinInfeccion;
        ((MovimientoEnemigo)m_e).gameObject.tag = "NPC";


    }
    public override void Actualizar()
    {
        base.Actualizar();
        
        if (((MovimientoEnemigo)m_e).EstaInfectado)
        {
            m_e.CambiarEstado(((MovimientoEnemigo)m_e)._ENestado_idle);
            return;
        }

        if (((MovimientoEnemigo)m_e).infectadocerca())
        {
            m_e.CambiarEstado(((MovimientoEnemigo)m_e)._estado_escape);
            ((MovimientoEnemigo)m_e).EstaInfectado = true;
            return;
        }
        
        ((MovimientoEnemigo)m_e).MovimientoRandom(1);
        
        
    }
    public override void Terminar()
    {
        base.Terminar();
    }
    





}
