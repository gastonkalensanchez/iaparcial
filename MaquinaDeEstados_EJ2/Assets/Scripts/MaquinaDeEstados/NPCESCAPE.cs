using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCESCAPE : EnemigoEstadoBase
{
    public NPCESCAPE(MovimientoEnemigo m_e):base("npcescape",m_e){}
    public override void Inicio()
    {
        base.Inicio();
    }
    public override void Actualizar()
    {
        base.Actualizar();
        if (!((MovimientoEnemigo)m_e).infectadocerca())
        {
            m_e.CambiarEstado(((MovimientoEnemigo)m_e)._estado_idle);
            return;
        }if (((MovimientoEnemigo)m_e).EstaInfectado)
        {
            m_e.CambiarEstado(((MovimientoEnemigo)m_e)._ENestado_idle);
            return;
        }
        else
        {
            ((MovimientoEnemigo)m_e).MovimientoRandom(1);
            ((MovimientoEnemigo)m_e).transformacioninfectado();
        }

    }
    public override void Terminar()
    {
        base.Terminar();
    }

}
