using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instancia;
    public List<GameObject> Infectados = new List<GameObject>();
    public List<GameObject> NPCs = new List<GameObject>();
    void Start()
    {
        if (instancia == null) instancia = this;
    }
    void Update()
    {
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("NPC"))
        {
            if (!(NPCs.Contains(item)))
            {
                NPCs.Add(item);
            }
            
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("INFECTADO"))
        {
            if (!(Infectados.Contains(item)))
            {
                Infectados.Add(item);
            }
        }
       
    }

    public void AgregarInfectado(GameObject nuevoinfectado)
    {
        Infectados.Add(nuevoinfectado);
        NPCs.Remove(nuevoinfectado);
    }
    public void AgregarNPC(GameObject NPC)
    {
        NPCs.Add(NPC);
        Infectados.Remove(NPC);

    }


 
}
