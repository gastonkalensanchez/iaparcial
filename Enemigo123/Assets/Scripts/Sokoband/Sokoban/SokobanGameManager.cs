﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
public class SokobanGameManager : MonoBehaviour
{
    Nivel nivel, nivelAux; // nivel
    GameObject casillero, casilleroTarget, pared, jugador, bloque,pisomalo; //los prefabs
    public List<Vector2> posOcupadasEsperadasCasillerosTarget;
    Stack pilaTablerosAnteriores = new Stack();

    string orientacionJugador;
    string nombreNivelActual = "Nivel2";

    bool gameOver = false;
    bool estoyDeshaciendo = false;


    private void Start()
    {
        casillero = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Casillero");
        pared = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Pared");
        jugador = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Jugador");
        bloque = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Bloque");

        CargarNivel(nombreNivelActual);

        //TestInstancia();
        StartCoroutine("MoverJugador");
        
    }

    private void CargarNivel(string nombre)
    {
        nivel = SokobanLevelManager.instancia.dameNivel(nombre);
        posOcupadasEsperadasCasillerosTarget = nivel.Tablero.damePosicionesObjetos("CasilleroTarget");
        for (int i = 0; i < SokobanLevelManager.instancia.mapa.width; i++)
        {
            for (int j = 0; j < SokobanLevelManager.instancia.mapa.height; j++)
            {

                InstanciadorPrefabs.instancia.graficarObjetosImagen(i, j, SokobanLevelManager.instancia.mapa, SokobanLevelManager.instancia.colormappings);
                
            }
        }
        InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
    }

    private void Update()
    {
        if (!gameOver)
        {
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                orientacionJugador = "derecha";
                mover();
            }
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                orientacionJugador = "arriba";
                mover();
            }
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                orientacionJugador = "izquierda";
                mover();
            }
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                orientacionJugador = "abajo";
                mover();
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                estoyDeshaciendo = true;
                mover();
            }
        }
       
    }

    private void mover()
    {

        //gameOver = ChequearVictoria(nivel.Tablero);
        if (estoyDeshaciendo == false)
        {

            Tablero tablAux = new Tablero(nivel.Tablero.casilleros.GetLength(0), nivel.Tablero.casilleros.GetLength(1));

            tablAux.setearObjetos(casillero, nivel.Tablero.damePosicionesObjetos("Casillero"));
            //tablAux.setearObjetos(casilleroTarget, nivel.Tablero.damePosicionesObjetos("CasilleroTarget"));
            tablAux.setearObjetos(bloque, nivel.Tablero.damePosicionesObjetos("Bloque"));
            tablAux.setearObjetos(pared, nivel.Tablero.damePosicionesObjetos("Pared"));
            tablAux.setearObjetos(jugador, nivel.Tablero.damePosicionesObjetos("Jugador"));
            //tablAux.setearObjetos(pisomalo, nivel.Tablero.damePosicionesObjetos("Pisomalo"));


            // punto 4 deshacer movimientos pulsando la Z, debemos utilizar un Stack 
            // pop es delete y push es add

            Vector2 posicionJugador = new Vector2(nivel.Tablero.damePosicionObjeto("Jugador").x, nivel.Tablero.damePosicionObjeto("Jugador").y);
            GameObject objActual, objProximo, objProximoProximo;
            objActual = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 0);
            objProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 1);
            objProximoProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 2);

           
            if (objProximo != null && objProximo.CompareTag("casillero"))
            {
                nivel.Tablero.setearObjeto(casillero, posicionJugador);
                nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 1);
                pilaTablerosAnteriores.Push(tablAux);
            }
            
            else
            {
                if (objProximo != null && objProximo.CompareTag("bloque"))
                {
                    nivel.Tablero.setearObjeto(casillero, posicionJugador);
                    nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 1);
                    pilaTablerosAnteriores.Push(tablAux);
                    
                }
            }
            
            InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
           
        }
        else 
        {
            if (pilaTablerosAnteriores.Count > 0)
            {
                Tablero tablero = (Tablero)pilaTablerosAnteriores.Pop();


                nivel.Tablero.setearObjetos(casillero, tablero.damePosicionesObjetos("Casillero"));
                nivel.Tablero.setearObjetos(casilleroTarget, tablero.damePosicionesObjetos("CasilleroDash"));
              
                nivel.Tablero.setearObjetos(bloque, tablero.damePosicionesObjetos("Bloque"));
                nivel.Tablero.setearObjetos(pared, tablero.damePosicionesObjetos("Pared"));
                nivel.Tablero.setearObjetos(jugador, tablero.damePosicionesObjetos("Jugador"));

                InstanciadorPrefabs.instancia.graficarObjetosTablero(tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());

            }
            else
            {
                InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
            }







            estoyDeshaciendo = false;



        }
    }

    private bool SonIgualesLosVectores(Vector2 v1, Vector2 v2)
    {
        return (v1.x == v2.x && v1.y == v2.y);
    }


    IEnumerator MoverJugador()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        orientacionJugador = "derecha";
        mover();


    }


    
}

