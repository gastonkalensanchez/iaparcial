﻿using UnityEngine;
using System.Collections.Generic;

public class SokobanLevelManager : MonoBehaviour
{

    public int posxbloque,posybloque;


    //Variable game objet donde en el inspector va a mostrar las que pide
    public GameObject casillero;
    public GameObject jugador;
    public GameObject bloque;
    public GameObject pared;
    //Afecta el valor de todas las instancias haciendo que valgan igual.
    //en este caso instancia a el mismo script
    public static SokobanLevelManager instancia;

    //level manager
    public Texture2D mapa;
    public ColorPrefab[] colormappings;


    void Awake()
    {
        // si es que no tiene valor la instancia 
        // se le asigna la instancia sino se destruye
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //no se destruye el objeto al cargar la esena
        DontDestroyOnLoad(gameObject);
    }

    public List<GameObject> dameLstPrefabsSokoban()//creo una lista para pedir prefabs
    {
        List<GameObject> lstPrefabsSokoban = new List<GameObject>();
        lstPrefabsSokoban.Add(casillero);
        lstPrefabsSokoban.Add(jugador);
        lstPrefabsSokoban.Add(pared);
        lstPrefabsSokoban.Add(bloque);
        //se le agrega a la lista los objetos declarados. y retorna la lista creada.
        return lstPrefabsSokoban;
    }
                
    

    private Tablero dameTablero2(int x, int y)
    {
        //pido otro tablero
        Tablero tablero = new Tablero(x, y);

        for (int i = 0; i < tablero.casilleros.GetLength(0); i++) 
        {
            for (int j = 0; j < tablero.casilleros.GetLength(1); j++)
            {
                tablero.setearObjeto(casillero, new Vector2(i, j));
            }
        }

        return tablero;
    }

    public Nivel dameNivel(string nombre)
    {
        return SokobanLevelManager.instancia.dameNiveles().Find(x => x.Nombre == nombre);//esta encontrando depende el lvl nombrado
    }

    private List<Nivel> dameNiveles()
    {
        List<Nivel> lstNiveles = new List<Nivel>(); //lista de los niveles
        lstNiveles.Add(new Nivel("Nivel2", SokobanLevelManager.instancia.dameTableroNivel2()));
        return lstNiveles;
    }

     
    private Tablero dameTableroNivel2()
    {
        Tablero tablero = dameTablero2(mapa.width, mapa.height);
        for (int x = 0; x < mapa.width; x++)
        {
            for (int y = 0; y < mapa.height; y++)
            {
                
                InstanciadorPrefabs.instancia.graficarObjetosImagen(x, y, mapa, colormappings);
               
            }
        }
        tablero.setearObjeto(pared, new Vector2(0,0));
        tablero.setearObjeto(pared, new Vector2(0,15));
        tablero.setearObjeto(pared, new Vector2(1,1));
        tablero.setearObjeto(pared, new Vector2(2,2));
        tablero.setearObjeto(pared, new Vector2(2,4));
        tablero.setearObjeto(pared, new Vector2(2, 6));
        tablero.setearObjeto(pared, new Vector2(2,14));
        tablero.setearObjeto(pared, new Vector2(3,3));
        tablero.setearObjeto(pared, new Vector2(3, 6));
        tablero.setearObjeto(pared, new Vector2(3,14));
        tablero.setearObjeto(pared, new Vector2(4,11));
        tablero.setearObjeto(pared, new Vector2(5,10));
        tablero.setearObjeto(pared, new Vector2(5,12));
        tablero.setearObjeto(pared, new Vector2(6,13));
        tablero.setearObjeto(pared, new Vector2(6,14));
        tablero.setearObjeto(pared, new Vector2(7,7));
        tablero.setearObjeto(pared, new Vector2(7,15));
        tablero.setearObjeto(pared, new Vector2(8,6));
        tablero.setearObjeto(pared, new Vector2(9,0));
        tablero.setearObjeto(pared, new Vector2(9,1));
        tablero.setearObjeto(pared, new Vector2(9,6));
        tablero.setearObjeto(pared, new Vector2(9,13));
        tablero.setearObjeto(pared, new Vector2(10,7));
        tablero.setearObjeto(pared, new Vector2(10,12));
        tablero.setearObjeto(pared, new Vector2(10,14));
        tablero.setearObjeto(pared, new Vector2(11,1));
        tablero.setearObjeto(pared, new Vector2(11,8));
        tablero.setearObjeto(pared, new Vector2(11,12));
        tablero.setearObjeto(pared, new Vector2(12, 1));
        tablero.setearObjeto(pared, new Vector2(12,7));
        tablero.setearObjeto(pared, new Vector2(12,12));
        tablero.setearObjeto(pared, new Vector2(13,7));
        tablero.setearObjeto(pared, new Vector2(13,13));
        tablero.setearObjeto(pared, new Vector2(14,7));
        tablero.setearObjeto(pared, new Vector2(14,14));
        tablero.setearObjeto(pared, new Vector2(15,7));
        tablero.setearObjeto(pared, new Vector2(15,14));
        tablero.setearObjeto(jugador, new Vector2(2, 1));
        if (posxbloque > 15 || posxbloque < 0 || posybloque > 15 || posybloque < 0)
        {
             tablero.setearObjeto(bloque, new Vector2(15,15));
        }else
        {
            tablero.setearObjeto(bloque, new Vector2(posxbloque,posybloque));
        }
        

        return tablero;
    }

   

}


