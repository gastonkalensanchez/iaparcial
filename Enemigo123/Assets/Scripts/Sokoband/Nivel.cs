﻿
    public class Nivel
    {
        public string Nombre { get; set; } //nombre
        public Tablero Tablero { get; set; }//tablero

        public Nivel(string nombre, Tablero tablero)
        {
            Nombre = nombre;
            Tablero = tablero;
        }
    }
