using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableroManager : MonoBehaviour
{
    public GameObject[,] Tablero = new GameObject[8, 8];
    public int tamañoDelTablero;
    public GameObject Casillero,objeto;
    public Vector3 posj;
    void Start()
    {
        tamañoDelTablero = 8;
        CrearTablero();
        InstanciarObjetos(objeto, new Vector2(5, 5));

    }
    private void CrearTablero()
    {
        for (int x = 0; x < Tablero.GetLength(0); x++)
        {
            for (int y = 0; y < Tablero.GetLength(1); y++)
            {
                // Crear una casilla
                Instantiate(Casillero, new Vector3(x, 0.0f, y), Quaternion.identity);
            }
        }
    }
    public void InstanciarObjetos(GameObject pObjeto, Vector2 pUbicacion)
    {
        objeto = Instantiate(objeto,new Vector3(pUbicacion.x - 0.13f,0.555f,pUbicacion.y - 0.71f),Quaternion.identity);
    }

    private void MoverJugador(Vector3 dirección)
    {
        // Calcular la nueva posición del jugador
        Vector3 nuevaPosición = objeto.transform.position + dirección * 1;
        objeto.transform.position = nuevaPosición;

        // Comprobar si la nueva posición está dentro de los límites del tablero
        if (nuevaPosición.x >= 0 && nuevaPosición.x < tamañoDelTablero * 1 &&
            nuevaPosición.z >= 0 && nuevaPosición.z < tamañoDelTablero * 1)
        {
            objeto.transform.position = nuevaPosición;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MoverJugador(Vector3.forward); //chequear esto y despues hacer todo con transform teniendo
           // 2 metodos que sea moverse para X o Y y hacer un chekeo
        }
    }
}
