﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinMaxTateti
{
    public partial class Form1 : Form
    {
        public int posicionesOcupadas;
        public int[,] Posiciones = new int[3, 3];
        public int[] IAUltimoMov = new int[3];
        // IA 1 Jugador 0
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {


            X00.Click += ToqueUnBotton;
            X01.Click += ToqueUnBotton;
            X02.Click += ToqueUnBotton;
            X10.Click += ToqueUnBotton;
            X11.Click += ToqueUnBotton;
            X12.Click += ToqueUnBotton;
            X20.Click += ToqueUnBotton;
            X21.Click += ToqueUnBotton;
            X22.Click += ToqueUnBotton;
            IniciarNuevaPartida();
            JuegoIA();
        }

        private void ToqueUnBotton(Object sender, EventArgs e)
        {
            ((Button)sender).Text = "X";
            if (((Button)sender).Name == "X00")
                JugarPosicion(0, 0);
            if (((Button)sender).Name == "X01")
                JugarPosicion(0, 1);
            if (((Button)sender).Name == "X02")
                JugarPosicion(0, 2);
            if (((Button)sender).Name == "X10")
                JugarPosicion(1, 0);
            if (((Button)sender).Name == "X11")
                JugarPosicion(1, 1);
            if (((Button)sender).Name == "X12")
                JugarPosicion(1, 2);
            if (((Button)sender).Name == "X20")
                JugarPosicion(2, 0);
            if (((Button)sender).Name == "X21")
                JugarPosicion(2, 1);
            if (((Button)sender).Name == "X22")
                JugarPosicion(2, 2);
            ((Button)sender).Enabled = false;
        }

        public void Actualizar()
        {
            if (Posiciones[0, 0] == 1)
            {
                X00.Text = "O";
                X00.Enabled = false;
            }
            if (Posiciones[0, 1] == 1)
            {
                X01.Text = "O";
                X01.Enabled = false;
            }
            if (Posiciones[0, 2] == 1)
            {
                X02.Text = "O";
                X02.Enabled = false;
            }
            if (Posiciones[1, 0] == 1)
            {
                X10.Text = "O";
                X10.Enabled = false;
            }
            if (Posiciones[1, 1] == 1)
            {
                X11.Text = "O";
                X11.Enabled = false;
            }
            if (Posiciones[1, 2] == 1)
            {
                X12.Text = "O";
                X12.Enabled = false;
            }
            if (Posiciones[2, 0] == 1)
            {
                X20.Text = "O";
                X20.Enabled = false;
            }
            if (Posiciones[2, 1] == 1)
            {
                X21.Text = "O";
                X21.Enabled = false;
            }
            if (Posiciones[2, 2] == 1)
            {
                X22.Text = "O";
                X22.Enabled = false;
            }

        }
        public void IniciarNuevaPartida()
        {
            for (int i = 0; i < Posiciones.GetLength(0); i++)
                for (int j = 0; j < Posiciones.GetLength(1); j++)
                    Posiciones[i, j] = -1;
        }

        public void JugarPosicion(int posx, int posy)
        {
            Posiciones[posx, posy] = 0;
            JuegoIA();
            Actualizar();
        }

        public bool PosicionesLleno()
        {
            bool x0 = false, x1 = false, x2 = false;

            if (Posiciones[0, 0] != 0 && Posiciones[0, 1] != 0 && Posiciones[0, 2] != 0)
            {
                x0 = true;
            }
            if (Posiciones[1, 0] != 0 && Posiciones[1, 1] != 0 && Posiciones[1, 2] != 0)
            {
                x1 = true;
            }
            if (Posiciones[2, 0] != 0 && Posiciones[2, 1] != 0 && Posiciones[2, 2] != 0)
            {
                x2 = true;
            }

            if (x0 && x1 && x2)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        public bool PuedeGanar(int jugador)
        {
            // Verificar filas
            for (int i = 0; i < 3; i++)
            {
                if (Posiciones[i, 0] == jugador && Posiciones[i, 1] == jugador && Posiciones[i, 2] == 0)
                {
                    Posiciones[i, 2] = -1;
                    Actualizar();
                    return true;
                }
                if (Posiciones[i, 0] == jugador && Posiciones[i, 1] == 0 && Posiciones[i, 2] == jugador)
                {

                    Posiciones[i, 1] = -1;
                    Actualizar();
                    return true;
                }
                if (Posiciones[i, 0] == 0 && Posiciones[i, 1] == jugador && Posiciones[i, 2] == jugador)
                {
                    Posiciones[i, 0] = -1;
                    Actualizar();
                    return true;
                }
            }

            // Verificar columnas
            for (int j = 0; j < 3; j++)
            {
                if (Posiciones[0, j] == jugador && Posiciones[1, j] == jugador && Posiciones[2, j] == 0)
                {
                    Posiciones[2, j] = -1;
                    Actualizar();
                    return true;
                }
                if (Posiciones[0, j] == jugador && Posiciones[1, j] == 0 && Posiciones[2, j] == jugador)
                {
                    Posiciones[1, j] = -1;
                    Actualizar();
                    return true;
                }
                if (Posiciones[0, j] == 0 && Posiciones[1, j] == jugador && Posiciones[2, j] == jugador)
                {
                    Posiciones[0, j] = -1;
                    Actualizar();
                    return true;
                }
            }

            // Verificar diagonales
            if (Posiciones[0, 0] == jugador && Posiciones[1, 1] == jugador && Posiciones[2, 2] == 0)
            {
                Posiciones[2, 2] = -1;
                Actualizar();
                return true;
            }
            if (Posiciones[0, 0] == jugador && Posiciones[1, 1] == 0 && Posiciones[2, 2] == jugador)
            {
                Posiciones[1, 1] = -1;
                Actualizar();
                return true;
            }
            if (Posiciones[0, 0] == 0 && Posiciones[1, 1] == jugador && Posiciones[2, 2] == jugador)
            {
                Posiciones[0, 0] = -1;
                Actualizar();
                return true;
            }
            if (Posiciones[0, 2] == jugador && Posiciones[1, 1] == jugador && Posiciones[2, 0] == 0)
            {
                Posiciones[2, 0] = -1;
                Actualizar();
                return true;
            }
            if (Posiciones[0, 2] == jugador && Posiciones[1, 1] == 0 && Posiciones[2, 0] == jugador)
            {
                Posiciones[1, 1] = -1;
                Actualizar();
                return true;
            }
            if (Posiciones[0, 2] == 0 && Posiciones[1, 1] == jugador && Posiciones[2, 0] == jugador)
            {
                Posiciones[0, 2] = -1;
                Actualizar();
                return true;
            }

            return false;
        }
        public void JuegoIA()
        {
            Actualizar();

            int f = 0;
            int c = 0;
            int valorJugada = int.MinValue;
            int auxiliar;

            for (int i = 0; i < Posiciones.GetLength(0); i++)
                for (int j = 0; j < Posiciones.GetLength(1); j++)
                    if (Posiciones[i, j] == -1)
                    {
                        Posiciones[i, j] = 1;
                        auxiliar = Mini();
                        if (auxiliar > valorJugada)
                        {
                            valorJugada = auxiliar;
                            f = i;
                            c = j;
                        }

                        Posiciones[i, j] = -1;
                    }
            Posiciones[f, c] = 1;
            IAUltimoMov[0] = f;
            IAUltimoMov[1] = c;


            Actualizar();
        }
        private int Max()
        {

            int valorJugada = int.MinValue;
            int auxiliar;
            for (int i = 0; i < Posiciones.GetLength(0); i++)
                for (int j = 0; j < Posiciones.GetLength(1); j++)
                    if (Posiciones[i, j] == -1)
                    {
                        Posiciones[i, j] = 1;
                        auxiliar = Mini();
                        if (auxiliar > valorJugada)
                            valorJugada = auxiliar;

                        Posiciones[i, j] = -1;
                    }

            return valorJugada;
        }
        private int Mini()
        {


            int valorJugada = int.MaxValue;
            int auxiliar;
            for (int i = 0; i < Posiciones.GetLength(0); i++)
                for (int j = 0; j < Posiciones.GetLength(1); j++)
                    if (Posiciones[i, j] == -1)
                    {
                        Posiciones[i, j] = 0;
                        auxiliar = Max();
                        if (auxiliar < valorJugada)
                            valorJugada = auxiliar;

                        Posiciones[i, j] = -1;
                    }

            return valorJugada;
        }








        public void IA2()
        {
            if (PosicionesLleno())
            {
                Actualizar();
                return;
            }

            // Verificar si la IA puede ganar en el siguiente movimiento

            // Verificar si el jugador puede ganar en el siguiente movimiento
            if (PuedeGanar(1))
            {
                Actualizar();
                return;
            }

            Random random = new Random();
            int x, y;
            x = random.Next(3);
            y = random.Next(3);

            if (Posiciones[x, y] == 0)
            {
                Posiciones[x, y] = -1;
            }
            else
            {
                while (Posiciones[x, y] != 0)
                {
                    x = random.Next(3);
                    y = random.Next(3);
                }
                Posiciones[x, y] = -1;
            }

            Actualizar();

        }
        public void JuegaIA1()
        {
            Actualizar();
            if (PosicionesLleno())
            {
                Actualizar();
                return;
            }

            Random random = new Random();
            int x, y;
            x = random.Next(3);
            y = random.Next(3);


            if (Posiciones[x, y] == 0)
            {
                Posiciones[x, y] = -1;
            }
            else
            {

                while (Posiciones[x, y] != 0)
                {
                    x = random.Next(3);
                    y = random.Next(3);
                }
                Posiciones[x, y] = -1;


            }


            Actualizar();


        }







    }
}

