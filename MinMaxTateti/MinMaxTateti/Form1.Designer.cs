﻿
namespace MinMaxTateti
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.X00 = new System.Windows.Forms.Button();
            this.X10 = new System.Windows.Forms.Button();
            this.X20 = new System.Windows.Forms.Button();
            this.X01 = new System.Windows.Forms.Button();
            this.X11 = new System.Windows.Forms.Button();
            this.X12 = new System.Windows.Forms.Button();
            this.X02 = new System.Windows.Forms.Button();
            this.X21 = new System.Windows.Forms.Button();
            this.X22 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // X00
            // 
            this.X00.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X00.Location = new System.Drawing.Point(12, 43);
            this.X00.Name = "X00";
            this.X00.Size = new System.Drawing.Size(70, 74);
            this.X00.TabIndex = 0;
            this.X00.UseVisualStyleBackColor = true;
            // 
            // X10
            // 
            this.X10.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X10.Location = new System.Drawing.Point(88, 43);
            this.X10.Name = "X10";
            this.X10.Size = new System.Drawing.Size(70, 74);
            this.X10.TabIndex = 1;
            this.X10.UseVisualStyleBackColor = true;
            // 
            // X20
            // 
            this.X20.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X20.Location = new System.Drawing.Point(164, 43);
            this.X20.Name = "X20";
            this.X20.Size = new System.Drawing.Size(70, 74);
            this.X20.TabIndex = 2;
            this.X20.UseVisualStyleBackColor = true;
            // 
            // X01
            // 
            this.X01.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X01.Location = new System.Drawing.Point(12, 123);
            this.X01.Name = "X01";
            this.X01.Size = new System.Drawing.Size(70, 74);
            this.X01.TabIndex = 3;
            this.X01.UseVisualStyleBackColor = true;
            // 
            // X11
            // 
            this.X11.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X11.Location = new System.Drawing.Point(88, 123);
            this.X11.Name = "X11";
            this.X11.Size = new System.Drawing.Size(70, 74);
            this.X11.TabIndex = 4;
            this.X11.UseVisualStyleBackColor = true;
            // 
            // X12
            // 
            this.X12.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X12.Location = new System.Drawing.Point(164, 123);
            this.X12.Name = "X12";
            this.X12.Size = new System.Drawing.Size(70, 74);
            this.X12.TabIndex = 5;
            this.X12.UseVisualStyleBackColor = true;
            // 
            // X02
            // 
            this.X02.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X02.Location = new System.Drawing.Point(12, 203);
            this.X02.Name = "X02";
            this.X02.Size = new System.Drawing.Size(70, 74);
            this.X02.TabIndex = 6;
            this.X02.UseVisualStyleBackColor = true;
            // 
            // X21
            // 
            this.X21.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X21.Location = new System.Drawing.Point(88, 203);
            this.X21.Name = "X21";
            this.X21.Size = new System.Drawing.Size(70, 74);
            this.X21.TabIndex = 7;
            this.X21.UseVisualStyleBackColor = true;
            // 
            // X22
            // 
            this.X22.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X22.Location = new System.Drawing.Point(164, 203);
            this.X22.Name = "X22";
            this.X22.Size = new System.Drawing.Size(70, 74);
            this.X22.TabIndex = 8;
            this.X22.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 345);
            this.Controls.Add(this.X22);
            this.Controls.Add(this.X21);
            this.Controls.Add(this.X02);
            this.Controls.Add(this.X12);
            this.Controls.Add(this.X11);
            this.Controls.Add(this.X01);
            this.Controls.Add(this.X20);
            this.Controls.Add(this.X10);
            this.Controls.Add(this.X00);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button X00;
        private System.Windows.Forms.Button X10;
        private System.Windows.Forms.Button X20;
        private System.Windows.Forms.Button X01;
        private System.Windows.Forms.Button X11;
        private System.Windows.Forms.Button X12;
        private System.Windows.Forms.Button X02;
        private System.Windows.Forms.Button X21;
        private System.Windows.Forms.Button X22;
    }
}

